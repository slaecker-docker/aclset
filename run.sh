#!/bin/sh

ACLDIR="/data"

if [ ! "$(ls -A ${ACLDIR})" ]; then
    echo "WARNING: Directory is empty"
    echo "         Mount a path or volume to /data"
fi

if [ ! -z $ACLUID ]; then
    echo "Setting acl rwx recursively for user $ACLUID"
    setfacl -Rm "u:${ACLUID}:rwx" ${ACLDIR}
    setfacl -Rdm "u:${ACLUID}:rwx" ${ACLDIR}
else
    echo "Variable ACLUID empty, won't set user acl"
fi

if [ ! -z $ACLGID ]; then
    echo "Setting acl rwx recursively for group $ACLGID"
    setfacl -Rm "g:${ACLGID}:rwx" ${ACLDIR}
    setfacl -Rdm "g:${ACLGID}:rwx" ${ACLDIR}
else
    echo "Variable ACLGID empty, won't set group acl"
fi

exit
