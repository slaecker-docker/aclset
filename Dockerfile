FROM arm32v6/alpine:edge

RUN echo "Downloading latest qemu-user-static" \
    && curl -o /usr/bin/qemu-user-static -L $(curl -fsSLI -o /dev/null -w %{url_effective} https://github.com/multiarch/qemu-user-static/releases/latest | sed 's!/tag/!/download/!')/qemu-arm-static \
    && echo "Installing acl" \
    && apk add --no-cache acl

COPY run.sh /usr/local/bin/

ENTRYPOINT /bin/sh
CMD /usr/local/bin/run.sh
